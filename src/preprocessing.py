import pandas as pd


class DataPreprocessor:
    def __init__(self, candidates, column_mappings):
        self.candidates = candidates
        self.column_mappings = column_mappings

    @staticmethod
    def _process_column_names(data_frame):
        data_frame.columns = [column_name.replace('\n', '') for column_name in data_frame.columns]

    @staticmethod
    def _get_turnout(data_frame):
        data_frame[u'turnout'] = data_frame[u'number_of_voted'] / data_frame[u'number_of_electorate']

    def _get_vote_percentage(self, data_frame):
        for candidate in self.candidates:
            data_frame[u'percentage_{}'.format(candidate)] = data_frame[candidate] / data_frame[u'number_of_voted']

    def process(self, data_frame):
        self._process_column_names(data_frame)
        data_frame = data_frame.replace('\n', 0)
        data_frame.dropna(inplace=True)
        # data_frame.drop(columns=['Дата протоколу'], inplace=True)
        data_frame.rename(columns=self.column_mappings, inplace=True)
        data_frame[[u'number_of_voted', u'number_of_electorate'] + self.candidates] = \
            data_frame[[u'number_of_voted', u'number_of_electorate'] + self.candidates].apply(pd.to_numeric)
        self._get_turnout(data_frame)
        self._get_vote_percentage(data_frame)
        return data_frame
