from matplotlib import pyplot as plt
import numpy as np
import os


class FraudAnalyzer:
    def __init__(self, name, data, candidates):
        self.data = data
        self.name = name
        self.bin_step = 0.01
        self.bins_array = np.arange(0, 1+self.bin_step, self.bin_step)
        self.candidates = candidates
        self.results_folder = u'../results/{}/'.format(self.name)
        self._create_results_folder()

    def _create_results_folder(self):
        if not os.path.exists(u'../results/'):
            os.mkdir(u'../results/')
        if not os.path.exists(self.results_folder):
            os.mkdir(self.results_folder)

    def _get_number_of_voted_vs_turnout(self):
        counts = list()
        for bin in self.bins_array:
            data_subset = self.data[(self.data[u'turnout'] >= bin) &
                                    (self.data[u'turnout'] < bin + self.bin_step)]
            counts.append(sum(data_subset['number_of_voted']))
        return counts

    def _get_votes_count_by_turnout(self, candidate):
        votes_count_by_turnout = list()
        for bin in self.bins_array:
            data_subset = self.data[(self.data[u'turnout'] >= bin) &
                                    (self.data[u'turnout'] < bin + self.bin_step)]
            votes_count_by_turnout.append(sum(data_subset[candidate]))
        return votes_count_by_turnout

    def _get_vote_count_by_candidate_percentage(self, candidate):
        votes_count_by_percentage = list()
        for bin in self.bins_array:
            data_subset = self.data[(self.data[u'percentage_{}'.format(candidate)] >= bin) &
                                    (self.data[u'percentage_{}'.format(candidate)] < bin + self.bin_step)]
            votes_count_by_percentage.append(sum(data_subset[candidate]))
        return votes_count_by_percentage

    def _get_number_polling_stations_by_candidate_percentage(self, candidate):
        number_polling_stations = list()
        for bin in self.bins_array:
            data_subset = self.data[(self.data[u'percentage_{}'.format(candidate)] >= bin) &
                                    (self.data[u'percentage_{}'.format(candidate)] < bin + self.bin_step)]
            number_polling_stations.append(data_subset[candidate].shape[0])
        return number_polling_stations

    def get_stats_at_100_percent_turnout(self):
        turnout_100 = self.data[self.data[u'turnout'] == 1]
        for candidate in self.candidates:
            plt.hist(turnout_100['percentage_{}'.format(candidate)], 50)
            title = u'Розподіл {} голосів за {} при явці 100%'.format(sum(turnout_100[candidate]), candidate)
            plt.title(title)
            plt.xlabel('Відсоток голосів за {}'.format(candidate))
            plt.ylabel('Кількість дільниць')
            plt.savefig('{}{}.jpg'.format(self.results_folder, title))
            # plt.show()
            plt.close()

    def plot_votes_count_by_turnout(self):
        for candidate in self.candidates:
            votes_count_by_turnout = self._get_votes_count_by_turnout(candidate)
            plt.plot(self.bins_array, votes_count_by_turnout, label=candidate)
        plt.xlabel('Явка на дільниці')
        plt.ylabel('Кількість голосів')
        plt.legend()
        plt.savefig('{}/Кількість голосів за певної явки.jpg'.format(self.results_folder))
        plt.close()
        plt.show()

    def plot_number_polling_stations_vs_candidate_percentage(self):
        for candidate in self.candidates:
            number_polling_stations = self._get_number_polling_stations_by_candidate_percentage(candidate)
            plt.plot(self.bins_array, number_polling_stations, label=candidate)
        plt.xlabel('Відсоток голосів за кандидата')
        plt.ylabel('Кількість дільниць')
        plt.legend()
        plt.savefig('{}/Кількість дільниць за певного відсотка голосів.jpg'.format(self.results_folder))
        plt.close()
        # plt.show()

    def plot_candidate_results_vs_canidate_percentage(self):
        for candidate in self.candidates:
            votes_count_by_percentage = self._get_vote_count_by_candidate_percentage(candidate)
            plt.plot(self.bins_array, votes_count_by_percentage, label=candidate)
        plt.xlabel('Відсоток голосів за кандидата')
        plt.ylabel('Кількість голосів')
        plt.legend()
        plt.savefig('{}Кількість голосів за певного відсотка голосів.jpg'.format(self.results_folder))
        plt.close()
        # plt.show()

    def plot_turnout_distribution(self):
        counts = self._get_number_of_voted_vs_turnout()
        plt.plot(self.bins_array, counts)
        plt.xlabel('Явка')
        plt.ylabel('Кількість голосів')
        plt.savefig('{}Загальна кількість голосів за певної явки.jpg'.format(self.results_folder))
        plt.close()
        # plt.show()

    def plot_turnout_vs_votes_percent(self):
        for candidate in self.candidates:
            plt.hist2d(self.data[u'turnout'], self.data[u'percentage_{}'.format(candidate)], 100)
            plt.xlabel('Явка')
            plt.ylabel('Вісоток голосів за {}'.format(candidate))
            # plt.show()
            plt.savefig('{}Відсоток голосів проти явки {}.jpg'.format(self.results_folder, candidate))
            plt.close()
