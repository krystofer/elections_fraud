from src.parsing_tools import HTMLTableParser
import os
import pandas as pd

from src.preprocessing import DataPreprocessor


class CECParser:
    """Парсить дані з ЦВК"""
    def __init__(self, url, name, candidates, column_mappings):
        self.url = url
        self.name = name
        self.candidates = candidates
        self.column_mappings = column_mappings
        self.html_table_parser = HTMLTableParser()
        self.n_electoral_disricts = 226
        self._create_data_folder()

    @staticmethod
    def _create_data_folder():
        if not os.path.exists(u'../data/'):
            os.mkdir(u'../data/')

    def _process_data_frame(self, data_frame):
        # data_frame.rename(columns=data_frame.iloc[0], inplace=True)
        data_frame.drop(0, axis=0, inplace=True)
        # data_frame = data_frame.apply(pd.to_numeric)
        return data_frame

    def get_data_from_csv(self):
        data = pd.read_csv(u'../data/{}.csv'.format(self.name))
        # data.dropna(inplace=True)
        # data = DataPreprocessor(self.candidates, self.column_mappings).process(data)
        return data

    def get_elections_data_from_csv(self):
        all_data_frames_list = []
        for i in range(1, self.n_electoral_disricts + 1):
            data_frame = pd.read_csv(u'data/e_district_{}.csv'.format(i))
            all_data_frames_list.append(data_frame)
        all_data = pd.concat(all_data_frames_list)
        all_data.to_csv(u'data/{}.csv'.format(self.name), index=False)
        return all_data

    def get_elections_data_from_cec_website(self):
        all_data_frames_list = []
        for i in range(1, self.n_electoral_disricts + 1):
            print(u'Обробляю дані з виборчого округу №{}'.format(i))
            url = self.url + str(i) + '.html'
            try:
                data_frame = self.html_table_parser.parse_url(url)[0][1]
                data_frame = self._process_data_frame(data_frame)
                # data_frame.to_csv(u'data/e_district_{}.csv'.format(i), index=False)
                all_data_frames_list.append(data_frame)
            except:
                print('Неможливо отримати дані з виборчого округу №{}'.format(i))
                # raise
        all_data = pd.concat(all_data_frames_list)
        data_preprocessed = DataPreprocessor(self.candidates, self.column_mappings).process(all_data)
        data_preprocessed.to_csv(u'../data/{}.csv'.format(self.name), index=False)
        return all_data

    def get_data(self):
        try:
            return self.get_data_from_csv()
        except:
            print('Cannot read data from csv. Attempting to get data from the CEC website')
            return self.get_elections_data_from_cec_website()


