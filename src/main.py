from src.fraud_analysis import FraudAnalyzer
from src.get_elections_data import CECParser


class Elections:
    def __init__(self, name, url, candidates, column_mappings):
        self.name = name
        self.url = url
        self.candidates = candidates
        self.column_mappings = column_mappings
        self.cec_parser = CECParser(
            url,
            name,
            candidates,
            column_mappings
        )

    def fraud_analysis(self):
        data = self.cec_parser.get_data()
        fraud_analyser = FraudAnalyzer(
            self.name,
            data,
            self.candidates
        )
        fraud_analyser.plot_turnout_distribution()
        fraud_analyser.plot_votes_count_by_turnout()
        fraud_analyser.plot_number_polling_stations_vs_candidate_percentage()
        fraud_analyser.plot_candidate_results_vs_canidate_percentage()
        fraud_analyser.plot_turnout_vs_votes_percent()
        fraud_analyser.get_stats_at_100_percent_turnout()


elections = {
    'presidential_2004_second_round': Elections(
        name='presidential_2004_second_round',
        url=u'http://www.cvk.gov.ua/pls/vp2004/wp336?pt001f01=502&pt005f01=',
        candidates=[
            u'Янукович',
            u'Ющенко'
        ],
        column_mappings={
            '2. Кількість виборців': 'number_of_electorate',
            '5. Взяли участь': 'number_of_voted'
        }
    ),
    'presidential_2010_second_round': Elections(
        name='presidential_2010_second_round',
        url=u'http://www.cvk.gov.ua/pls/vp2010/wp336?pt001f01=701&pt005f01=',
        candidates=[
            u'Янукович',
            u'Тимошенко'
        ],
        column_mappings={
            '1. Кількість виборців': 'number_of_electorate',
            '2. Взяли участь': 'number_of_voted'
        }
    ),
    'presidential_2014_first_round': Elections(
        name='presidential_2014_first_round',
        url=u'http://www.cvk.gov.ua/pls/vp2014/wp336?pt001f01=702&pt005f01=',
        candidates=[
            u'Порошенко',
            u'Тимошенко',
            u'Гриценко'
        ],
        column_mappings={
            '2.К-сть виборців, внесених до списку': 'number_of_electorate',
            '9.К-сть виборців, які взяли участь у голосуванні': 'number_of_voted'
        }
    ),
    'presidential_2019_first_round': Elections(
        name='presidential_2019_first_round',
        url=u'https://www.cvk.gov.ua/pls/vp2019/wp336pt001f01=719pt005f01=',
        candidates=[
            u'Порошенко',
            u'Тимошенко',
            u'Зеленський'
        ],
        column_mappings={
            '2.К-сть виборців, внесених до списку': 'number_of_electorate',
            '9.К-сть виборців, які взяли участь у голосуванні': 'number_of_voted'
        }
    ),
    'presidential_2019_second_round': Elections(
        name='presidential_2019_second_round',
        url=u'https://www.cvk.gov.ua/pls/vp2019/wp336pt001f01=720pt005f01=',
        candidates=[
            u'Порошенко Петро',
            u'Зеленський Володимир'
        ],
        column_mappings={
            '2.К-сть виборців, внесених до списку': 'number_of_electorate',
            '9.К-сть виборців, які взяли участь у голосуванні': 'number_of_voted',
            u'ПорошенкоПетро': u'Порошенко Петро',
            u'ЗеленськийВолодимир': u'Зеленський Володимир'
        }
    )
}

if __name__ == '__main__':
    # elections['presidential_2004_second_round'].fraud_analysis()
    # elections['presidential_2010_second_round'].fraud_analysis()
    # elections['presidential_2014_first_round'].fraud_analysis()
    # elections['presidential_2019_first_round'].fraud_analysis()
    elections['presidential_2019_second_round'].fraud_analysis()
